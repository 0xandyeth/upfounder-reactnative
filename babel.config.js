module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./app'],
        alias: {
          '@api': './app/api',
          '@components': './app/components',
          '@screens': './app/screens',
          '@constant': './app/constant',
          '@types': './app/types',
          '@helpers': './app/helpers',
          '@navigation': './app/navigation',
          '@redux': './app/redux',
          '@styles': './app/styles',
          '@assets': './app/assets',
          '@utils': './app/utils'
        }
      }
    ]
  ]
}
