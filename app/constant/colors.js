// Colors

const Color = {
  primaryColor: '#3DB6FB',
  primaryDarkColor: '#007582',

  transparent: 'transparent',

  black: '#1d1d1d',
  black0: '#00000000',
  black10: '#00000019',
  blackf0: '#000000f0',
  black100: '#222c31',

  white: '#fff',
  whitea0: '#fffff0a0',
  gray200: '#606060',
  gray400: '#7a7a7a',
  gray430: '#808080',
  gray450: '#8b9ea5',
  gray500: '#78849E',
  gray50050: '#0178849E',
  gray800: '#d2d5d7',
  gray850: '#D6DAE2',
  gray900: '#DCE1E4',
  gray930: '#F4F4F6',
  gray950: '#F8F9FA',
  gray960: '#FBFCFE',

  lightGray: '#78849E',
  green: '#13DBB5',
  green10: '#13DBB519',
  green20: '#13DBB533',
  yellow: '#FFBC43',
  yellow10: '#FFBC4319',
  yellow20: '#FFBC4333',
  yellow80: '#FFBC43CC',
  yellow40: '#FFBC4366',
  blue: '#3DB6FB',
  blue10: '#3DB6FB19',
  blue20: '#3DB6FB33',
  blue40: '#3DB6FB66',
  blue70: '#3DB6FBB2',
  blue80: '#3DB6FBCC',
  red: '#F5634B',
  red10: '#F5634B19',
  red20: '#F5634B33',
  pink: '#FF326E',
  pink10: '#FF326E19',
  darkBlue: '#0D5ADA',
  darkBlue10: '#0D5ADA19',
  darkBlue20: '#0D5ADA33',
  darkblue80: '#0D5ADACC'
}

export default Color
