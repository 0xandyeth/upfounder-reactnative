import moment from 'moment'
export const services = ['Curbside Delivery', 'In-Home/Backyard Delivery', 'Full Service Delivery']
export const returnServices = ['Curbside/Driveway', 'In-Home/Backyard']
export const assemblyServices = ['In-Home/Backyard']
export const floors = [
  'No lifting up any stairs, Ground level delivery',
  'Lifting up 1 flight of stairs ',
  'Lifting up 2 flights of stairs'
]
export const return_floors = [
  'Located On Main Level(no stairs)',
  'Located One Level Above Ground',
  'Located Two Levels Above Ground'
]

export const teams = [
  { label: 'None', value: 0 },
  { label: 'Alpha', value: 1 },
  { label: 'Beta', value: 2 },
  { label: 'Charlie', value: 3 },
  { label: 'Delta', value: 4 },
  { label: 'Echo', value: 5 }
]

export const publish = {
  status: 'test',// live or test
  storeURL: {
    android: 'https://play.google.com/store/apps/details?id=com.cdi.customerapp&hl=en_US',
    ios: 'https://apps.apple.com/us/app/cdi-services/id1491698113'
  },
  version: {
    android: '1.0',
    ios: '1.1'
  }
}


const temp = new Date()
const tmr = new Date(temp.getTime() + 24 * 60 * 60 * 1000);
const fromW = new Date(temp.getTime() - 3 * 24 * 60 * 60 * 1000);
const toW = new Date(temp.getTime() + 3 * 24 * 60 * 60 * 1000);
export const today = moment(temp).format('YYYY-MM-DD')
export const tomorrow = moment(tmr).format('YYYY-MM-DD')

export const fromWeek = moment(fromW).format('YYYY-MM-DD')
export const toWeek = moment(toW).format('YYYY-MM-DD')
export const dayOfTmr = tmr.getDay();
export const nextDate = (previous) => {
  const previoustime = new Date(previous)
  const nextTime = new Date(previoustime.getTime() + 2 * 24 * 60 * 60 * 1000);
  return moment(nextTime).format('YYYY-MM-DD')
}

export const getDay = (value) => {
  const previoustime = new Date(value)
  return previoustime.getDay();
}