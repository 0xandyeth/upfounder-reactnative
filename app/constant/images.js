const Images = {  
  LogoUrl: require('@assets/logo.png'),
  VerifyAddress: require('@assets/verify-address.png'),
  defaultAvatar:'https://upfounders.herokuapp.com/static/media/marc.8880a65c.jpg'
}

export default Images
