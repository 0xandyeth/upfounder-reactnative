const Css = {
  position: {
    absolute: 'absolute',
    center: 'center'
  },
  zIndex: {
    z1: 1,
    z100: 100,
    z1000: 1000
  },
  weight: {
    bold: 'bold',
    weight100: '100',
    weight200: '200',
    weight300: '300',
    weight400: '400',
    weight500: '500',
    weight600: '600',
    weight700: '700',
    weight800: '800',
    weight900: '900'
  },
  flex: {
    row: 'row',
    flex02: 0.2,
    flex03: 0.3,
    flex1: 1,
    flex2: 2,
    flex3: 3,
    flex4: 4,
    flex5: 5,
    flex6: 6,
    flex7: 7,
    flex8: 8,
    flex9: 9
  }
}

export default Css
