
import WService from '@helpers/WebService'
var wservice = new WService()


export const signIn = (email, password) => {
    return new Promise((resolve, reject) => {
        wservice.signIn(email, password)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body.success) {
                    resolve(response.body)
                    console.log(response.body)
                } else {
                    reject(response.body)
                }
            })
            .catch(reject)
    })
}
export const signUp = (email, password, name) => {
    return new Promise((resolve, reject) => {
        wservice.signUp(email, password, name)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body.email) {
                    resolve(response.body.email)
                } else {
                    reject(response.body)
                }
            })
            .catch(reject)
    })
}

export const forgotPassword = (email) => {
    return new Promise((resolve, reject) => {
        wservice.forgotPassword(email)
            .then((response) => {
                if (response.statusCode === 200 && response.body) {
                    resolve(response.body)
                } else {
                    reject(new Error(response.body))
                }
            })
            .catch(reject)
    })
}

export const myProfile = () => {
    return new Promise((resolve, reject) => {
        wservice.myProfile()
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body) {
                    resolve(response.body)
                } else {
                    reject(new Error(response.body.message))
                }
            })
            .catch(reject)
    })
}
export const profile = (id) => {
    return new Promise((resolve, reject) => {
        wservice.profile(id)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body) {
                    resolve(response.body)
                } else {
                    reject(new Error(response.body.message))
                }
            })
            .catch(reject)
    })
}

export const getContact = (id) => {
    return new Promise((resolve, reject) => {
        wservice.getContact(id)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body) {
                    resolve(response.body)
                } else {
                    reject(new Error(response.body.message))
                }
            })
            .catch(reject)
    })
}

export const getContactByCategory = (category, userId) => {
    return new Promise((resolve, reject) => {
        wservice.getContactByCategory(category, userId)
            .then((response) => {
                console.log(response)
                if (response.statusCode === 200 && response.body.docs) {
                    resolve(response.body.docs)
                } else {
                    reject(new Error(response.body.message))
                }
            })
            .catch(reject)
    })
}


