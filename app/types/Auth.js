const AuthUser = {
  loading: 'loading',
  error: 'error',
  errorMessage: 'errorMessage',
  appRole: 'appRole',
  orderType: 'orderType'
}

export default AuthUser
