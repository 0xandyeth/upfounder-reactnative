const Profile = {
  firstName: 'firstName',
  lastName: 'lastName',
  membership: 'membership',
  deliveryAddress: 'deliveryAddress',
  cellPhone: 'cellPhone',
  homePhone: 'homePhone',
  email: 'email'
}

export default Profile
