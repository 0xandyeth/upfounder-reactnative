import { AppButton, SafeView, Header, EditInput, ProfileAvatar } from '@components/index'
import { DefaultStyles, Images, Screens, Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { ImageBackground, Image, Text, View, AsyncStorage, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
import { Icon, Content } from 'native-base'
import ImagePicker from "react-native-customized-image-picker";
import { fileEndpoint } from '@api/Endpoint'
import { handleCheckPermissions } from '@utils/permission'
import axios from 'axios'

const SubDot = <Icon type={'FontAwesome5'} name={'dot-circle'} style={{ fontSize: Dimensions.px14, marginBottom: Dimensions.px10 }} />

const categories = [
    { label: 'Mentors', value: 1 },
    { label: 'Investors', value: 2, },
    { label: 'Other Founders', value: 3 },
    { label: 'Developers', value: 4 },
]

class ContactPage extends Component {
    componentDidMount() {
        handleCheckPermissions()
        this.sub = this.props.navigation.addListener('didFocus', this.handleInit)
    }
    componentWillUnmount() {
        if (this.sub) this.sub.remove()
    }
    handleInit = () => {
        const linkedinUrl = this.props.navigation.getParam('linkedinUrl', null)
        if (linkedinUrl) {
            const profile = this.props.AuthUser.profile
            this.props.updateAuthDetail('profile', { ...profile, linkedinUrl })
        }
    }
    verifyProfile() {
        const userId = this.props.AuthUser.profile._id
        let objCopy = Object.assign({}, this.props.AuthUser.profile);
        console.log(objCopy);
        axios
            .post(`${fileEndpoint}/users/edit/${userId}`, objCopy)
            .then(res => {
                console.log('Succes==s>>', res.body)
                NavigationService.goBack()
            })
            .catch(err => {
                console.log('ERROR=>>edit', err)
            })
    }
    changeAvatar() {
        const profile = this.props.AuthUser.profile
        ImagePicker.openPicker({
            multiple: false
        }).then(contactUrl => {
            this.props.updateAuthDetail('profile', { ...profile, contactUrl })
            console.log(images);
        });
    }
    render() {
        const { updateAuthDetail } = this.props
        const profile = this.props.AuthUser.profile
        const { phone, email, name, linkedinUrl, contactUrl, photoUrl, aboutMe, category, _id, } = profile
        const selectedCategory = categories.find(item => item.value == category)
        return (
            <SafeView bgColor={Color.primaryColor}>
                <Header
                    type={'back'}
                    title={'EDIT PROFILE'}
                />
                <Content width={'100%'}>
                    <View style={styles.container}>
                        <View style={styles.topContainer}>
                            <ProfileAvatar
                                source={photoUrl || contactUrl && contactUrl.path || Images.defaultAvatar}
                                size={Dimensions.px100}
                                disable
                                style={{ marginTop: 20 }}
                            />
                            <Text style={styles.px20Bold}>{name}</Text>
                            {/* {aboutMe && <Text style={styles.px20Bold}>{aboutMe}</Text>}                             */}
                        </View>
                        <EditInput
                            name={'Full Name'}
                            value={name}
                            width={'85%'}
                            onChangeText={name => updateAuthDetail('profile', { ...profile, name })}
                        />
                        <EditInput
                            name={'Email'}
                            value={email}
                            width={'85%'}
                            onChangeText={email => updateAuthDetail('profile', { ...profile, email })}
                        />
                        <EditInput
                            name={'Phone'}
                            value={phone}
                            width={'85%'}
                            onChangeText={phone => updateAuthDetail('profile', { ...profile, phone })}
                        />
                        <View style={styles.flexContainer}>
                            <View style={{ width: '85%', marginLeft: -10 }}>
                                <EditInput
                                    name={'Linkedin Url'}
                                    value={linkedinUrl}
                                    width={'100%'}
                                    onChangeText={linkedinUrl => updateAuthDetail('profile', { ...profile, linkedinUrl })}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => NavigationService.navigate('Scanner', { goBack: Screens.EditContact })}
                            >
                                <Icon type={'MaterialCommunityIcons'} name={'qrcode-scan'} style={{ fontSize: Dimensions.px20, color: Color.primaryColor }} />
                            </TouchableOpacity>
                        </View>
                        <EditInput
                            name={'Mission'}
                            value={aboutMe}
                            width={'85%'}
                            onChangeText={aboutMe => updateAuthDetail('profile', { ...profile, aboutMe })}
                        />

                        <AppButton
                            width={'50%'}
                            name={'SAVE'}
                            mgTop={20}
                            onPress={() => this.verifyProfile()}
                        />

                    </View>
                </Content>
            </SafeView>
        )
    }
}
const mapStateToProps = ({ auth, contact }) => {
    return {
        AuthUser: auth,
        Contact: contact

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactPage)
