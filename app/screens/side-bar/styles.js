import { Color, Dimensions } from '@constant/index'
export default {
    container: {
        flex:1,
        alignItems: 'center',
        marginTop:Dimensions.px20
    },
    title: {
        color: Color.black,
        fontSize: Dimensions.px18,
        fontWeight: '800',
        margin: Dimensions.px20,
        textAlign: 'center'
    },
    list:{
        width:Dimensions.pro80
    },
    flexContainer: {
        width: Dimensions.pro90,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

}
