import { SideBarItem, SafeView, ProfileAvatar } from '@components/index'
import { DefaultStyles, Images, Color, Dimensions } from '@constant/index'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { FlatList, Image, AsyncStorage, Text } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import nameList from './data'
import styles from './styles'
class SidebarPage extends Component {
    async logout() {
        await AsyncStorage.removeItem("loginCompleted")
        this.props.logoutUser()
    }

    render() {
        const { name } = this.props.profile
        return (
            <SafeView>

                <ProfileAvatar
                    source={Images.defaultAvatar}
                    size={Dimensions.px100}
                    style={{ marginTop: 20 }}
                    disable
                />
                <Text style={styles.title}>{name}</Text>
                <FlatList
                    data={nameList}
                    style={styles.list}
                    renderItem={({ item }) =>
                        <SideBarItem
                            item={item}
                            logout={() => this.logout()}
                        />
                    }
                    keyExtractor={(item, index) => index.toString()}
                />


            </SafeView>
        )
    }

}


const mapStateToProps = ({ auth, }) => {
    return {
        profile: auth.profile
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SidebarPage)