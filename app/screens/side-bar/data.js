import { Screens } from '@constant/index';
export default [    
    {
        title: 'PROFILE',
        iconType: 'FontAwesome',
        iconName: 'user',
        navigateKey: Screens.Profile,
    },
    
    {
        title: 'LOG OUT',
        iconType: 'Entypo',
        iconName: 'log-out',
        navigateKey: Screens.Login,
    }  
    
]