import { Color, Dimensions } from '@constant/index'
export default {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Color.white,
    resizeMode: 'contain',
    justifyContent: 'center'
  },
  authBox: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.black10,
    width: Dimensions.pro100
  },
  authBtn: {
    backgroundColor: Color.primaryColor,
    width: Dimensions.px300,
    height: Dimensions.px50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Dimensions.px5,
    margin: Dimensions.px10,
    flexDirection:'row'
  },
  btnText: {
    color: Color.white,
    fontWeight: 'bold',
    fontSize: Dimensions.px20
  },
  forgotText: {
    width: Dimensions.px300,
    color: Color.black,
    textAlign:'right',
    fontSize: Dimensions.px14,
    marginBottom:10
  },
  animationBox: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  }
}
