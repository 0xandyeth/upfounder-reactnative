import { Color, Dimensions } from '@constant/index'
export default {
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    resizeMode: 'contain'
  },
  authBox: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Color.white,
    width: Dimensions.pro100
  },
  authBtn: {
    backgroundColor: Color.primaryColor,
    width: Dimensions.px300,
    height: Dimensions.px50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Dimensions.px5,
    margin: Dimensions.px10
  },
  btnText: {
    color: Color.white,
    fontWeight: 'bold',
    fontSize: Dimensions.px20
  },
  animationBox: {
    flex: 6,
    alignItems: 'center',
    justifyContent: 'center'
  }
}
