import { AppButton, Header, Link, ProfileAvatar, SafeView } from '@components/index'
import { Color, Dimensions, Images, Screens } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import { Content, Icon } from 'native-base'
import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
import { fileEndpoint } from '@api/Endpoint'
const SubDot = <Icon type={'FontAwesome5'} name={'dot-circle'} style={{ fontSize: Dimensions.px14, marginBottom: Dimensions.px10 }} />

const categories = [
    { label: 'Mentors', value: 1 },
    { label: 'Investors', value: 2, },
    { label: 'Other Founders', value: 3 },
    { label: 'Developers', value: 4 },
]

class ContactPage extends Component {
      
    
    render() {
        const { updateContactDetail } = this.props
        const { user } = this.props.Contact
        const { phone, email, name, linkedinUrl, contactUrl, photoUrl, category, keyStrength, note } = user
        const selectedCategory = categories.find(item => item.value == category)
        const avatar = photoUrl ? `${fileEndpoint}/${photoUrl}` : Images.defaultAvatar
        return (
            <SafeView bgColor={Color.primaryColor}>
                <Header
                    type={'back'}
                    title={'CONTACT DETAIL'}
                    onEdit={()=>NavigationService.navigate(Screens.EditContact)}
                />
                <Content width={'100%'}>
                    <View style={styles.container}>
                        <View style={styles.topContainer}>
                            <View style={styles.profile}>
                                <Text style={styles.px20Bold}>{selectedCategory && selectedCategory.label}</Text>
                                <ProfileAvatar
                                    source={contactUrl && contactUrl.uri || avatar}
                                    size={Dimensions.px100}
                                    disable
                                    style={{ marginTop: 20 }}
                                />
                            </View>
                            <View style={{ marginLeft: 20 }}>
                                <Text style={styles.px20Bold}>{name}</Text>
                            </View>
                        </View>
                        <Link
                            type={'email'}
                            iconType={'Zocial'}
                            iconName={'email'}
                            label={email}
                        />
                        <Link
                            type={'call'}
                            iconType={'Ionicons'}
                            iconName={'md-call'}
                            label={phone}
                        />
                        <Link
                            type={'url'}
                            iconType={'AntDesign'}
                            iconName={'linkedin-square'}
                            label={linkedinUrl}
                        />
                        <Text style={styles.subTitle}>Key Strength</Text>
                        {
                            keyStrength && keyStrength.map(strenth => {
                                if (strenth == '') return null
                                return <View style={styles.noteBox}>
                                    {SubDot}
                                    <Text style={styles.note}>{strenth}</Text>
                                </View>
                            })
                        }

                        <Text style={styles.subTitle}>Note</Text>
                        {
                            note && note.map(txt => {
                                if (txt == '') return null
                                return <View style={styles.noteBox}>
                                    {SubDot}
                                    <Text style={styles.note}>{txt}</Text>
                                </View>
                            })
                        }
                    </View>
                </Content>
            </SafeView>
        )
    }
}
const mapStateToProps = ({ auth, contact }) => {
    return {
        AuthUser: auth,
        Contact: contact

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactPage)
