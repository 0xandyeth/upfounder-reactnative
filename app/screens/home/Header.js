import { Color, Dimensions, Images } from '@constant/index'
import { Icon } from 'native-base'
import React, { Component } from 'react'
import { StyleSheet, TextInput, StatusBar, View, TouchableOpacity, Text, Image } from 'react-native'

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Color.primaryColor,
        height: Dimensions.px60,
        justifyContent: 'center',
        width: Dimensions.deviceWidth,
        marginBottom: 20
    },
    profileContainer: {
        left: 15,
        position: 'absolute',
        zIndex: 100
    },
    menuContainer: {
        right: 15,
        position: 'absolute',
        zIndex: 100
    },
    subContainer: {
        backgroundColor: Color.white,
        width: Dimensions.deviceWidth - 120,
        height: Dimensions.px40,
        borderRadius: Dimensions.px8,
        flexDirection: 'row',
        marginLeft: 10
    },
    profileImage: {
        width: Dimensions.px35,
        height: Dimensions.px35,
        borderRadius: Dimensions.px35 / 2,
        backgroundColor: Color.white,
    },
    input: {
        color: Color.black,
        fontSize: Dimensions.px15,
        marginLeft: 40,
        width: Dimensions.deviceWidth - 200,
    },
    search: {
        position: 'absolute',
        left: 10,
        alignSelf: 'center',
    },
    close: {
        position: 'absolute',
        right: 10,
        alignSelf: 'center',
    }

})

export default class HeaderBar extends Component {
    render() {
        let { onChangeText, value, onScanner, onProfile, onPress } = this.props
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={Color.primaryColor} barStyle='light-content' />
                <TouchableOpacity
                    style={styles.profileContainer}
                    onPress={() => onProfile && onProfile()}

                >
                    <Image
                        style={styles.profileImage}
                        source={{ uri: Images.defaultAvatar }}
                    />

                </TouchableOpacity>
                <View style={styles.subContainer}>
                    <TouchableOpacity
                        onPress={() => { onPress && onPress() }}
                        style={styles.search}
                    >
                        <Icon
                            type={'FontAwesome'}
                            name={'search'}
                            style={{ fontSize: Dimensions.px20 }}
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={styles.input}
                        autoCapitalize='none'
                        placeholder={'Search'}
                        returnKeyType="search"
                        onChangeText={text => {
                            onChangeText && onChangeText(text)
                        }}
                        onSubmitEditing={() => onPress && onPress()}
                        value={value ? value.toString() : value}
                    />
                    {
                        Boolean(value) && <TouchableOpacity
                            onPress={() => {
                                onChangeText && onChangeText(null);
                                onPress && onPress();
                            }}
                            style={styles.close}
                        >
                            <Icon
                                type={'AntDesign'}
                                name={'close'}
                                style={{ fontSize: Dimensions.px20 }}
                            />
                        </TouchableOpacity>
                    }
                </View>
                <TouchableOpacity
                    style={styles.menuContainer}
                    onPress={() => onScanner && onScanner()}
                >
                    <Icon type={'MaterialCommunityIcons'} name={'qrcode-scan'}  style={{ color: Color.white, fontSize: Dimensions.px25 }} />
                </TouchableOpacity>
            </View>
        )
    }
}
