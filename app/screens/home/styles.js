import { Color, Dimensions } from '@constant/index'
export default {
  container: {
    width: Dimensions.pro100,
    height: Dimensions.pro100,
    alignItems: 'center'
  },
  flexContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    borderBottomWidth: 0.5,
    padding: 10,
    borderBottomColor: Color.primaryColor
  },
  title: {
    fontSize: Dimensions.px20,
    color: Color.primaryColor,
    fontWeight: 'bold',
    width: '85%'
  },
  add: {
    postion: 'absolute',
    right: 10,
    backgroundColor: Color.primaryColor,
    padding: 5,
    width: Dimensions.px50,
    alignItems: 'center',
    borderRadius: 10
  },
  flex: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subContainer: {
    width: '90%',
    margin: 10
  },
  subTitle: {
    fontSize: Dimensions.px18,
    fontWeight: 'bold'
  },
  avatar: {
    width: Dimensions.px40,
    height: Dimensions.px40,
    borderRadius: Dimensions.px20,
    borderWidth: 2,
    borderColor: Color.primaryColor
  },
  userInformation: {
    paddingLeft: 10
  },
  name: {
    fontSize: Dimensions.px14,
    fontWeight: 'bold',
    marginBottom: 5
  },
  note: {
    fontSize: Dimensions.px12,
  },
  dropheader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  addContact: {
    shadowColor: Color.black,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    backgroundColor: Color.white,
    width: 50,
    height: 50,
    borderRadius: 25,
    postion: 'absolute',
    right: -Dimensions.deviceWidth / 2 + 40,
    bottom: 65,
    alignItems: 'center',
    justifyContent: 'center'
  }
}
