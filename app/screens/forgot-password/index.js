import * as Services from '@api/index';
import { AppButton, Header, EditInput, SafeView } from '@components/index';
import NavigationService from '@navigation/NavigationService.js'
import { Color, Dimensions, Screens } from '@constant/index';
import { Content } from 'native-base';
import React, { Component } from 'react';
import { View , Text} from 'react-native';
import styles from './styles';

class ForgotPasswordPage extends Component {
    state = {
        email: null,
        isLoading: false,
        message: null,
        request: 'start',
        buttonLabel: 'Submit'
    }
    handleForgotpassword() {
        let { email, isLoading } = this.state
        if (!email) return 0
        this.setState({ isLoading: !isLoading })
        Services.forgotPassword(email.toLowerCase())
            .then((message) => {
                this.setState({
                    isLoading: false,
                    request: 'success',
                    buttonLabel: 'Go To Sign In',
                    message: `We've just sent an email to reset your password in Up Founders Application`
                })
                console.log({ message })
            })
            .catch((errMsg) => {
                this.setState({
                    isLoading: false,
                    request: 'resend',
                    buttonLabel: 'Resend',
                    message: "Sorry, Not account found"
                })
                console.log({ errMsg })
            })
    }
    handleSubmit() {
        const { request } = this.state
        if (request === 'start') {
            this.handleForgotpassword()
        } else if (request === 'success') {
            NavigationService.goBack()
        } else if (request === 'resend') {
            this.setState({
                request: 'start',
                buttonLabel: 'Submit',
                message: null
            })
        }

    }
    render() {
        let { email, isLoading, message , buttonLabel} = this.state
        return (
            <SafeView bgColor={Color.primaryColor}>
                <Header type={'back'} title={'Forgot Password'} />
                <Content width={Dimensions.pro100}>
                    <View style={styles.alignCenter}>
                        {
                            message ? <Text style={styles.message}>{message}</Text> :
                                <EditInput
                                    name={'Enter Your Email Address'}
                                    value={email}
                                    width={Dimensions.pro80}
                                    onChangeText={email => this.setState({ email })}
                                />
                        }
                        <AppButton
                            name={buttonLabel}
                            width={'80%'}
                            mgTop={30}
                            isLoading={isLoading}
                            onPress={() => this.handleSubmit()}
                        />
                    </View>
                </Content>
            </SafeView>
        )
    }
}

export default ForgotPasswordPage