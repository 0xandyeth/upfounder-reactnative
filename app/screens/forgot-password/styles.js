import { Color, Dimensions } from '@constant/index'
export default {
  alignCenter: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingTop: 20
  },
  flexContainer: {
    alignItems: 'center',
    width: Dimensions.pro90,
    justifyContent: 'space-between',
    flexDirection: 'row'
  }, 
  label:{
    fontSize: Dimensions.px14,
    width:Dimensions.pro100
  },
  message:{
      fontSize:Dimensions.px16,
      fontWeight:'bold',
      margin:Dimensions.px20
  }
}
