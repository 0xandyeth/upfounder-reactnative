import { AppButton, SafeView } from '@components/index'
import { DefaultStyles, Images, Screens, Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import React, { Component } from 'react'
import { ImageBackground, Image, Text, View, AsyncStorage, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'
import { Icon } from 'native-base'
import QRCodeScanner from 'react-native-qrcode-scanner';


class HomePage extends Component {
   
    componentDidMount() {

    }
    eventHandle(linkedinUrl) {        
        const goBack = this.props.navigation.getParam('goBack', Screens.Contact)
        NavigationService.navigate(goBack,{linkedinUrl})
    }
    render() {
       
        return (
            <SafeView bgColor={Color.transparent}>
                <Text
                    style={styles.cancelText}
                    onPress={() => { console.log('Go back') }}
                >Cancel</Text>
                <QRCodeScanner
                    showMarker={true}
                    onRead={(e) => this.eventHandle(e.data)}
                    cameraStyle={{ flex: 1 }}
                />
            </SafeView>
        )
    }
}
const mapStateToProps = ({ auth }) => {
    return {
        AuthUser: auth,

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage)
