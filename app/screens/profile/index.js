import { AppButton, Header, Link, ProfileAvatar, SafeView } from '@components/index'
import { Color, Dimensions, Images, Screens } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { ActionCreators } from '@redux/action.js'
import { Content, Icon } from 'native-base'
import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from './styles'

const SubDot = <Icon type={'FontAwesome5'} name={'dot-circle'} style={{ fontSize: Dimensions.px14, marginBottom: Dimensions.px10 }} />

const categories = [
    { label: 'Mentors', value: 1 },
    { label: 'Investors', value: 2, },
    { label: 'Other Founders', value: 3 },
    { label: 'Developers', value: 4 },
]

class ContactPage extends Component {


    render() {
        const profile = this.props.AuthUser.profile
        const { phone, email, name, linkedinUrl, contactUrl, photoUrl, aboutMe, category } = profile
        const selectedCategory = categories.find(item => item.value == category)
        const qrcodegenerater = `https://api.qrserver.com/v1/create-qr-code/?size=250x250&data=${linkedinUrl}`
        return (
            <SafeView bgColor={Color.primaryColor}>
                <Header
                    type={'back'}
                    title={'MY PROFILE'}
                    onEdit={()=>NavigationService.navigate(Screens.EditProfile)}
                />
                <Content width={'100%'}>
                    <View style={styles.container}>
                        <View style={styles.topContainer}>
                            <ProfileAvatar
                                source={photoUrl || contactUrl && contactUrl.path || Images.defaultAvatar}
                                size={Dimensions.px100}
                                disable
                                style={{ marginTop: 20 }}
                            />
                            <Text style={styles.px20Bold}>{name}</Text>
                            {/* {aboutMe && <Text style={styles.px20Bold}>{aboutMe}</Text>} */}
                        </View>
                        <Link
                            type={'email'}
                            iconType={'Zocial'}
                            iconName={'email'}
                            label={email}
                        />
                        <Link
                            type={'call'}
                            iconType={'Ionicons'}
                            iconName={'md-call'}
                            label={phone}
                        />
                        <Link
                            type={'url'}
                            iconType={'AntDesign'}
                            iconName={'linkedin-square'}
                            label={linkedinUrl}
                        />
                        <View width={'85%'}>
                            <Text style={styles.px20Bold}>{'Mission: '}</Text>
                            <Text style={{padding:20}}>{aboutMe}</Text>
                        </View>
                        <Image
                            source={{ uri: qrcodegenerater }}
                            style={{width:100,height:100,margin:20}}
                        />                      

                    </View>
                </Content>
            </SafeView>
        )
    }
}
const mapStateToProps = ({ auth, contact }) => {
    return {
        AuthUser: auth,
        Contact: contact

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactPage)
