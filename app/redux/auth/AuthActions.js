import { ENDPOINT } from '../../api/Endpoint.js'
import { ActionTypes } from '../ActionTypes'
import * as Services from '@api/index'
import NavigationService from '@navigation/NavigationService.js'
import { Screens } from '@constant/index'
import { AsyncStorage } from 'react-native'
import setAuthToken from "@utils/setAuthToken";
export const loginUser = ({ email, password }) => {
  return dispatch => {
    dispatch({ type: ActionTypes.LOGIN_USER_START })
    Services.signIn(email, password)
      .then((data) => {
        setAuthToken(data.token)
        loginUserSuccess(dispatch, data, email, password)
      })
      .catch((errMsg) => {
        loginUserFail(dispatch, 'Email or Password Incorrect!')
      })
  }
}
export const getProfile = () => (dispatch, getState) => {
  const { auth: { profile } } = getState()
  const { _id } = profile
  Services.profile(_id)
    .then((data) => {
      dispatch({
        type: ActionTypes.GET_PROFILE,
        payload: data
      })
    })
    .catch((errMsg) => {
      loginUserFail(dispatch, 'Unauthorized')
    })
}
export const myProfile = () => (dispatch) => {
  Services.myProfile()
    .then((data) => {
      console.log('myProfile', data.user)
      dispatch({
        type: ActionTypes.GET_PROFILE,
        payload: data.user
      })
    })
    .catch((errMsg) => {
      loginUserFail(dispatch, 'Unauthorized')
    })
}
export const getContact = () => (dispatch, getState) => {
  const { auth: { profile } } = getState()
  const { _id } = profile
  Services.getContact(_id)
    .then((data) => {
      dispatch({
        type: ActionTypes.GET_CONTACT,
        payload: data
      })
    })
    .catch((errMsg) => {
    })
}
export const getContactByCategory = (category, key) => (dispatch, getState) => {
  const { auth: { profile } } = getState()
  const { _id } = profile
  Services.getContactByCategory(category, _id)
    .then((data) => {
      dispatch({
        type: ActionTypes.GET_CONTACT_BY_CATEGORY,
        payload: {
          contact: data, key
        }
      })
    })
    .catch((errMsg) => {
    })
}


export const signUpUser = (email, password, name) => {
  return dispatch => {
    dispatch({ type: ActionTypes.LOGIN_USER_START })
    Services.signUp(email, password, name)
      .then((profile) => {
        loginUser({ email, password })
      })
      .catch((errMsg) => {
        //alert('An account for this email already exists.')
        console.log({ errMsg })
        loginUserFail(dispatch, errMsg == 'Email address exists in database.' ? errMsg : "Please Try Again.")
      })
  }
}

const loginUserFail = (dispatch, errorMessage) => {
  alert(errorMessage)
  dispatch({
    type: ActionTypes.LOGIN_USER_FAIL,
    payload: {
      errorMessage: errorMessage
    }
  })
}

const loginUserSuccess = (dispatch, data, email, password) => {
  AsyncStorage.setItem("loginCompleted", "true")
  AsyncStorage.setItem("email", email)
  AsyncStorage.setItem("password", password)
  dispatch({
    type: ActionTypes.LOGIN_USER_SUCCESS,
    payload: data
  })
  NavigationService.navigateAndReset(Screens.Home)

}

export const updateAuthDetail = (type, data) => {
  return dispatch => {
    dispatch({
      type: ActionTypes.UPDATE_AUTH_DETAIL,
      payload: {
        type,
        data
      }
    })
  }
}

export const logoutUser = () => {
  return dispatch => {
    dispatch({
      type: ActionTypes.LOG_OUT,
      payload: 'There was an error connection'
    })
    NavigationService.navigateAndReset(Screens.Login);
  }
}


