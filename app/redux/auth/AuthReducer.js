import { ActionTypes } from '../ActionTypes'
const defaultProfile = {
  name: null,
  _id: null,
  email: null,
  keyStrength: [],
  note: [],
  aboutCompany: null,
  aboutMe: null
}
export const AuthReducer = (
  state = {
    profile: defaultProfile,
    contact: [],
    mentors: [],
    founders: [],
    investors: [],
    developers: [],
    loading: false,
    loginCompleted: false,
    error: false,
    errorMessage: '',
    token: null
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.LOGIN_USER_START:
      return {
        ...state,
        error: false,
        loading: true,
        errorMessage: '',
        loginCompleted: false
      }

    case ActionTypes.LOGIN_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        loginCompleted: true,
        profile: action.payload.user,
        token: action.payload.token,
      }
    case ActionTypes.AUTO_LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        loginCompleted: true,
        profile: action.payload.user,
        token: action.payload.token,
      }

    case ActionTypes.LOGIN_USER_FAIL:
      return {
        ...state,
        error: true,
        loading: false,
        loginCompleted: false,
        errorMessage: action.payload.errorMessage
      }
    case ActionTypes.LOG_OUT:
      return {
        ...state,
        error: false,
        loginCompleted: false,
        token: null,
        profile: defaultProfile,
        errorMessage: action.payload
      }
    case ActionTypes.UPDATE_AUTH_DETAIL:
      return {
        ...state,
        [action.payload.type]: action.payload.data
      }
    case ActionTypes.GET_CONTACT:
      return {
        ...state,
        contact: action.payload
      }
    case ActionTypes.GET_CONTACT_BY_CATEGORY:
      return {
        ...state,
        [action.payload.key]: action.payload.contact
      }
    case ActionTypes.GET_PROFILE:
      return {
        ...state,
        profile: action.payload
      }

    default:
      return state
  }
}
