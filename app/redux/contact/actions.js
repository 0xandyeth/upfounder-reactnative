import { ENDPOINT } from '../../api/Endpoint.js'
import { ActionTypes } from '../ActionTypes'
import * as Services from '@api/index'
import NavigationService from '@navigation/NavigationService.js'
import { Screens } from '@constant/index'


export const updateContactDetail = (type, data) => {
    return dispatch => {
        dispatch({
            type: ActionTypes.UPDATE_CONTACT_DETAIL,
            payload: {
                type,
                data
            }
        })
    }
}

export const addContact = (data) => (dispatch) => {
    Services.addContact(data)
        .then((res) => {
            dispatch({
                type: ActionTypes.ADD_CONTACT,
                payload: res
            })
        })
        .catch((errMsg) => {
        })
}

export const clearContact = () => (dispatch) => {
    dispatch({ type: ActionTypes.CLEAR_CONTACT })
}
export const editContact = (data, key) => (dispatch) => {
    dispatch({ type: ActionTypes.EDIT_CONTACT, payload: data})
    NavigationService.navigate(key)
}

