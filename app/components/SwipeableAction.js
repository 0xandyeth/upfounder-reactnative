import React from 'react'
import { View, TouchableOpacity, Image, Animated, Text, StyleSheet } from 'react-native'
import { Icon } from 'native-base'
import { Color, Dimensions, Css } from '@constant/index'



const SwipeableAction = ({ text, icon, type, backgroundColor, position, progress, handleOnPress, size }) => {
  const trans = progress.interpolate({
    inputRange: [0, 1],
    outputRange: [position, 0]
  })

  const renderIcon = () => {
    return <Icon type={type} name={icon} style={{ fontSize: size || Dimensions.px25 }} color={'black'} />
  }

  return (
    <Animated.View style={[styles.action, { backgroundColor }, { transform: [{ translateX: trans }] }]}>
      <TouchableOpacity style={styles.button} onPress={() => handleOnPress(text)}>
        {renderIcon()}
        <Text style={styles.actionText}>{text}</Text>
      </TouchableOpacity>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  action: {
    flex: 1,
    width: 79,
    borderRadius: 10,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    margin: Dimensions.px5,
    marginBottom: Dimensions.px10,

  },
  actionText: {
    marginTop: 5,

    color: 'black'
  },
  imageContainer: {
    height: 20,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  image: {
    maxWidth: 20,
    maxHeight: 20
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})


SwipeableAction.defaultProps = {
  icon: null,
  image: null,
  containerStyle: {},
  handleOnPress: () => null
}

export default SwipeableAction
