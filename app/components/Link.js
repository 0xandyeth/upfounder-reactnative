import { Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, Linking } from 'react-native'
import { UIActivityIndicator } from 'react-native-indicators'
import { Icon } from 'native-base'
const styles = StyleSheet.create({
    Btn: {
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: Dimensions.px10,
        backgroundColor: Color.primaryColor,
        height: Dimensions.px40,
        paddingLeft: 10,
        marginBottom: Dimensions.px10,
        width: '85%'
    },
    btnText: {
        color: Color.white,
        fontWeight: '900',
        paddingLeft: 10
    }
})

export default class AppButton extends Component {
    onPress(type, label) {
        if (!label) return 0
        if (type == 'call') {
            Linking.openURL(`tel:${label}`)
        } else if (type == 'email') {
            Linking.openURL(`mailto:${label}?subject=Subject&body=Hi`)
        } else {
            Linking.openURL(label)
        }
    }
    render() {
        const { label, type, iconType, iconName } = this.props
        return (
            <TouchableOpacity
                style={styles.Btn}
                onPress={() => this.onPress(type, label)}
            >
                <Icon type={iconType} name={iconName} style={{ color: Color.white }} />
                <Text style={styles.btnText}>{label}</Text>
            </TouchableOpacity>
        )
    }
}
