import { Color, Dimensions } from '@constant/index'
import { Label } from 'native-base'
import React, { Component } from 'react'
import { View, TextInput } from 'react-native'

export default class EditInput extends Component {
  state = {
    labelColor: Color.black,
    fieldColor: Color.black10
  }
  render() {
    let { labelColor, fieldColor } = this.state
    let { onChangeText, name, isValid, value, width, keyboardType, secureTextEntry } = this.props
    return (
      <View
        style={{
          margin: Dimensions.px10,
          borderBottomColor: isValid === undefined ? fieldColor : Color.red,
          width: width === undefined ? Dimensions.pro80 : width,

        }}
      >
        <Label style={{ fontSize: Dimensions.px14, color: labelColor, marginBottom:5 , fontWeight:'bold'}}>{name}</Label>
        <TextInput
          style={{
            fontSize: Dimensions.px14,
            color: labelColor,
            padding: 0,
            paddingBottom:5,
            borderBottomColor: Color.gray450,
            borderBottomWidth: 1
          }}
          secureTextEntry={secureTextEntry}
          keyboardType={keyboardType || 'default'}
          onChangeText={text => onChangeText(text)}
          value={value}
        />
      </View>
    )
  }
}
