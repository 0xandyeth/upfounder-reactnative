import { Color, Dimensions, DefaultStyles } from '@constant/index';
import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: Dimensions.pro100,
        resizeMode: 'contain',
        width: Dimensions.pro100
    }
})

export default class CustomSafeView extends Component {
    render() {
        let { bgColor, children } = this.props
        return (
            <SafeAreaView style={[styles.container, { backgroundColor: bgColor || Color.gray960 }]}>
                <View style={[DefaultStyles.container, { backgroundColor: bgColor === Color.transparent ? Color.transparent : Color.gray960 }]}>
                    {children}
                </View>
            </SafeAreaView>
        )
    }
}
