import { Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { UIActivityIndicator } from 'react-native-indicators'

const styles = StyleSheet.create({
  Btn: {
    alignItems: 'center',
    borderRadius: Dimensions.px10,
    backgroundColor: Color.primaryColor,
    height: Dimensions.px35,
    justifyContent: 'center',
    marginBottom: Dimensions.px10
  },
  btnText: {
    color: Color.white,
    fontWeight: '900'
  }
})

export default class AppButton extends Component {
  render() {
    let { onPress, name, screenKey, width, fontSize, mgTop, isLoading, mgLeft, bgColor } = this.props
    return (
      <TouchableOpacity
        style={[styles.Btn,
        {
          width: width || Dimensions.pro80,
          marginTop: mgTop || Dimensions.px10,
          marginLeft: mgLeft || 0.1,
          backgroundColor: bgColor ? Color.gray900 : Color.primaryColor,
        }]}
        onPress={() => {
          !isLoading && onPress && onPress()
          screenKey && NavigationService.navigate(screenKey)
        }}
      >
        {isLoading ? (
          <UIActivityIndicator color={Color.primaryColor} size={Dimensions.px20} />
        ) : (
            <Text style={[styles.btnText, { fontSize: fontSize || Dimensions.px18, color: bgColor ? Color.black : Color.white, }]}>{name}</Text>
          )}
      </TouchableOpacity>
    )
  }
}
