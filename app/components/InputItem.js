import { Color, Dimensions } from '@constant/index'
import { Icon } from 'native-base'
import React, { Component } from 'react'
import { StyleSheet, TextInput, View } from 'react-native'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: Color.primaryColor,
    borderRadius: Dimensions.px5,
    borderWidth: Dimensions.px1,
    flexDirection: 'row',
    height: Dimensions.px50,
    justifyContent: 'center',
    margin: Dimensions.px10,
    paddingLeft: Dimensions.px25,
    paddingRight: Dimensions.px25,
    width: Dimensions.px300
  },
  iteminput: {
    color: Color.primaryColor,
    fontSize: Dimensions.px14,
    paddingLeft: Dimensions.px10,
    width: Dimensions.px250
  }
})

export default class InputItem extends Component {
  state = {
    passwordShow: true
  }

  render() {
    let { placeholder, IconType, IconName, onChangeText, value } = this.props
    let { passwordShow } = this.state
    if (IconName === 'lock') {
      return (
        <View style={styles.container}>
          <Icon type={IconType} name={IconName} style={{ fontSize: Dimensions.px20, color: Color.primaryColor }} />
          <TextInput
            style={[styles.iteminput, { width: Dimensions.px220 }]}
            underlineColorAndroid='transparent'
            placeholder={placeholder}
            placeholderTextColor={Color.primaryColor}
            autoCapitalize='none'
            returnKeyType={'done'}
            secureTextEntry={passwordShow}
            onChangeText={text => {
              onChangeText(text)
            }}
            value={value}
          />
          <Icon
            type={'Entypo'}
            name={passwordShow !== true ? 'eye' : 'eye-with-line'}
            style={{ fontSize: Dimensions.px25, color: Color.primaryColor }}
            onPress={() => {
              this.setState({ passwordShow: !passwordShow })
            }}
          />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <Icon type={IconType} name={IconName} style={{ fontSize: Dimensions.px20, color: Color.primaryColor }} />
          <TextInput
            style={styles.iteminput}
            underlineColorAndroid='transparent'
            placeholder={placeholder}
            placeholderTextColor={Color.primaryColor}
            autoCapitalize='none'
            onChangeText={text => {
              onChangeText(text)
            }}
            value={value}
          />
        </View>
      )
    }
  }
}
