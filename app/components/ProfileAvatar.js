import { Color, Dimensions } from '@constant/index'
import React, { Component } from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native'
import { UIActivityIndicator } from 'react-native-indicators'
import { Icon } from 'native-base'

const alignCenter = {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: Dimensions.px20
}
const buttonStyle = {
    backgroundColor: Color.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: -10,


}
export default class ChangeAvatarPage extends Component {
    render() {
        let { source, onChangeAvatar, size, style, disable } = this.props
        let buttonSize = size / 4
        return (
            <ImageBackground
                style={[{ height: size, width: size }, alignCenter, style ? style : {}]}
                imageStyle={{
                    borderRadius: size / 2,
                    backgroundColor: Color.gray930,
                    borderColor: Color.primaryColor,
                    borderWidth: 0.2
                }}
                source={{ uri: source }}
            >
                {
                    !disable &&
                    <TouchableOpacity
                        onPress={() => onChangeAvatar && onChangeAvatar()}
                        style={[{
                            height: buttonSize,
                            width: buttonSize,
                            borderRadius: buttonSize / 2,
                        }, buttonStyle]}
                    >

                        <Icon name={'camera'} type={'FontAwesome'} style={{ fontSize: buttonSize / 2, color: Color.white }} />
                    </TouchableOpacity>
                }
            </ImageBackground>
        )
    }
}
