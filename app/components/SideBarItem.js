import NavigationService from '@navigation/NavigationService'
import { Icon } from 'native-base'
import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { Dimensions, Color, Screens } from '@constant/index'

const styles = StyleSheet.create({
    container: {
        width: Dimensions.pro80,
        alignItems: 'center',
        flexDirection: 'row',
        margin: Dimensions.px20
    },
    label: {
        paddingLeft: Dimensions.px10,
        color: Color.primaryColor,
        fontSize: Dimensions.px18
    }
})
class SideBarItem extends React.Component {
    render() {
        const { logout, item: { navigateKey, iconType, iconName, title } } = this.props
        return (
            <TouchableOpacity style={styles.container}
                onPress={() => {
                    if (navigateKey == Screens.Login) {
                        logout && logout()
                    } else {                        
                        navigateKey && NavigationService.navigate(navigateKey)
                    }

                }}
            >
                <Icon
                    type={iconType}
                    name={iconName}
                    style={{ color: Color.primaryColor, fontSize: Dimensions.px25 }}
                />
                <Text style={styles.label}>{title}</Text>
            </TouchableOpacity>
        )
    }
}

export default SideBarItem
