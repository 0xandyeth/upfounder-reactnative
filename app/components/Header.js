import { Color, Dimensions } from '@constant/index'
import NavigationService from '@navigation/NavigationService.js'
import { Icon } from 'native-base'
import React, { Component } from 'react'
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { getLabel } from '@utils/index'
import { ActionCreators } from '@redux/action.js'
import { OrderTypes } from '@types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: Color.primaryColor,
        flexDirection: 'row',
        height: Dimensions.px50,
        justifyContent: 'center',
        width: Dimensions.pro100
    },
    menuIcon: {
        left: 15,
        position: 'absolute',
        zIndex: 100,
        width: Dimensions.px40,
        height: Dimensions.px30
    },
    editIcon: {
        right: 15,
        position: 'absolute',
        zIndex: 100,
        width: Dimensions.px30,
        height: Dimensions.px30
    },
    title: {
        color: Color.white,
        fontSize: Dimensions.px18,
        fontWeight: 'bold',
        position: 'absolute',
        textAlign: 'center',
        width: Dimensions.pro80,
        zIndex: 1
    },
})

class Header extends Component {
    render() {
        const { type, title, onPress, onEdit } = this.props

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={Color.primaryColor} barStyle='light-content' />
                <TouchableOpacity
                    style={styles.menuIcon}
                    onPress={() => (type !== 'menu' ? NavigationService.goBack() : onPress && onPress())}
                >
                    {type === 'noback' ? null : type !== 'menu' ? (
                        <Icon
                            name={'arrow-back'}
                            type={'MaterialIcons'}
                            style={{ color: Color.white, fontSize: Dimensions.px25 }}
                        />
                    ) : (
                            <Icon name={'menu'} type={'Entypo'} style={{ color: Color.white, fontSize: Dimensions.px25 }} />
                        )}
                </TouchableOpacity>
                <Text style={styles.title}>{title}</Text>
                {
                    onEdit &&
                    <TouchableOpacity
                        style={styles.editIcon}
                        onPress={() => onEdit()}
                    >
                        <Icon
                            name={'edit'}
                            type={'AntDesign'}
                            style={{ color: Color.white, fontSize: Dimensions.px25 }}
                        />
                    </TouchableOpacity>
                }
            </View>
        )
    }
}


const mapStateToProps = ({ auth }) => {

    return {
        auth
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(ActionCreators, dispatch)
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header)
