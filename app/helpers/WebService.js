import { ENDPOINT } from '@api/Endpoint'

import NetworkHelper from './NetworkHelper'

function WService() {
  this.url = ENDPOINT
}

WService.prototype.makeUrl = function (resource) {
  var url = this.url + resource
  return url
}


WService.prototype.signUp = function (email, password, name) {
  return NetworkHelper.requestPost(this.makeUrl('users/register'), { email, password, name })
}

WService.prototype.forgotPassword = function (email) {
  return NetworkHelper.requestPost(this.makeUrl('/mail/resetPasswordRequest'), { email })
}

WService.prototype.signIn = function (email, password) {
  return NetworkHelper.requestPost(this.makeUrl('users/login'), { email, password })
}
WService.prototype.profile = function (id) {
  return NetworkHelper.requestGet(this.makeUrl(`users/profile/${id}`))
}
WService.prototype.myProfile = function () {
  return NetworkHelper.requestGet(this.makeUrl(`users/MyProfile`))
}
//// Contact
WService.prototype.getContact = function (id) {
  return NetworkHelper.requestGet(this.makeUrl(`contact/${id}`))
}

WService.prototype.addContact = function (data) {
  return NetworkHelper.requestPost(this.makeUrl(`contact/addContact`, data))
}

WService.prototype.getContactByCategory = function (category, userId) {
  return NetworkHelper.requestGet(this.makeUrl(`contact/byCategory/${category}/${userId}`))
}
WService.prototype.editContact = function (id, data, token) {
  return NetworkHelper.requestPost(this.makeUrl(`contact/edit/${id}`, data))
}
WService.prototype.editPhoto = function (id, data) {
  return NetworkHelper.requestPost(this.makeUrl(`contact/editPhoto/${id}`, data))
}


module.exports = WService
