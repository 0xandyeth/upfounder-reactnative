
import { getDistanceFromCoords } from '@utils/location'
import { Alert } from 'react-native'


export const showAlert = (confirmText, title, message, onPress) =>
    Alert.alert(title, message, [{ text: confirmText, onPress: onPress || null }], { cancelable: false })

export const showCustomAlert = (title, message, cancelText, confirmText, onConfirm) =>
    Alert.alert(
        title,
        message,
        [{ text: cancelText, style: 'cancel' }, { text: confirmText, onPress: onConfirm || null }],
        { cancelable: false }
    )


