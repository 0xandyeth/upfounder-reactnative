import { showCustomAlert } from '@utils/index'
import { Platform } from 'react-native'
import { check, openSettings, PERMISSIONS, request, RESULTS } from 'react-native-permissions'

export const handleCheckPermissions = () => {
    setTimeout(async () => {
        if (Platform.OS === 'android') {
            const storagePermission = await check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
            if (storagePermission !== RESULTS.GRANTED) {
                await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
            }
        } else {
            const storagePermissionCamera = await check(PERMISSIONS.IOS.CAMERA)
            if (storagePermissionCamera !== RESULTS.GRANTED) {
                if (storagePermissionCamera === RESULTS.BLOCKED) {
                    handleOpenSetting(
                        `"UpFounders" Would Like to Access the Camera`,
                        'This app would like to access your camera to attach selected files to your contact.',
                    )
                } else {
                    await request(PERMISSIONS.IOS.CAMERA)
                }

            }
            const storagePermissionPhoto = await check(PERMISSIONS.IOS.PHOTO_LIBRARY)
            if (storagePermissionPhoto !== RESULTS.GRANTED) {
                if (storagePermissionPhoto === RESULTS.BLOCKED) {
                    handleOpenSetting(
                        `"UpFounders" Would Like to Access Your Photos`,
                        'This app would like to access your Photo Library to attach selected files to your contact.',
                    )
                } else {
                    await request(PERMISSIONS.IOS.PHOTO_LIBRARY)
                }
            }
            const storagePermissionPhotoSave = await check(PERMISSIONS.IOS.MICROPHONE)
            if (storagePermissionPhotoSave !== RESULTS.GRANTED) {
                if (storagePermissionPhotoSave === RESULTS.BLOCKED) {
                    handleOpenSetting(
                        `"UpFounders" Would Like to Use Your MicroPhone`,
                        'This app would like to use your Microphone to attach selected files to your contact.',
                    )
                } else {
                    await request(PERMISSIONS.IOS.MICROPHONE)
                }
            }
        }
    }, 1000)
}

const handleOpenSetting = (title, message) => {
    showCustomAlert(title, message, `Don't Allow`, 'OK', () => openSettings()
    )
}