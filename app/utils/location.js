
const toRad = value => (value * Math.PI) / 180
export const kilometersToMiles =km => km / 1.6

export const metersToMiles =meters => meters * 0.000621371192

export const metersToKilometers = meters => meters * 1000

export const getDistanceFromCoords = (   
    { latitude: latE, longitude: lonE },
    { latitude: latW, longitude: lonW },
    unit = 'mi'
) => {
    const EARTH_RADIUS = 6371 // km
    const dLat = toRad(latW - latE)
    const dLon = toRad(lonW - lonE)
    const lat1 = toRad(latE)
    const lat2 = toRad(latW)

    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2)
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    let distance = EARTH_RADIUS * c

    if (unit === 'm') {
        distance = metersToKilometers(distance)
    }

    if (unit === 'mi') {
        distance = kilometersToMiles(distance)
    }

    return distance.toFixed(1)
}
