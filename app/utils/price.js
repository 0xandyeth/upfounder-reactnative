export const getFees = (item, productPrices, newOrderType) => {

    let {
        price,
        itemId,

        numberOfBoxes,
        numberOfBoxesOver200lbs,
        assemblyCost,
        openBoxReturnCost,
        openBoxDisassemblyReturnCost,
    } = item
    let num200Under = numberOfBoxes - numberOfBoxesOver200lbs
    let num200Over = numberOfBoxesOver200lbs
    let {
        // FIrst Box Delivery
        fBD,
        fBDReturn,
        fBDOpenBox,
        // Each Additional Box
        eAB,
        eABReturn,
        eABO200lbs,
        eABOpenBox,
        // In-Home Each Box
        iHEBBFU200lbs,
        iHEBBFO200lbs,
        iHEB2FU200lbs,
        iHEB2FO200lbs,
        iHEB3FU200lbs,
        iHEB3FO200lbs,
        // In-Home Each Box when adding Assembly
        aIHEBBFU200lbs,
        aIHEBBFO200lbs,
        aIHEB2FU200lbs,
        aIHEB2FO200lbs,
        aIHEB3FU200lbs,
        aIHEB3FO200lbs
    } = productPrices


    let returnFee = fix(fBDReturn) + fix(eABReturn) * (fix(numberOfBoxes || 1) - 1) + fix(eABO200lbs) * fix(num200Over)
    let return_fir_add = fix(iHEBBFU200lbs) * fix(num200Under) + fix(iHEBBFO200lbs) * fix(num200Over)
    let return_sec_add = fix(iHEB2FU200lbs) * fix(num200Under) + fix(iHEB2FO200lbs) * fix(num200Over)
    let return_thi_add = fix(iHEB3FU200lbs) * fix(num200Under) + fix(iHEB3FO200lbs) * fix(num200Over)
    if (newOrderType === "return") {
        return {
            fee: returnFee,
            first: return_fir_add,
            second: return_sec_add,
            third: return_thi_add
        }
    }
    let fullService_Fee = fix(assemblyCost) + fix(eAB) * (fix(numberOfBoxes || 1) - 1) + fix(aIHEBBFO200lbs) * fix(num200Over)
    let fullService_fir_add = fix(aIHEBBFU200lbs) * fix(num200Under) + fix(aIHEBBFO200lbs) * fix(num200Over)
    let fullService_sec_add = fix(aIHEB2FU200lbs) * fix(num200Under) + fix(aIHEB2FO200lbs) * fix(num200Over)
    let fullService_thi_add = fix(aIHEB3FU200lbs) * fix(num200Under) + fix(aIHEB3FO200lbs) * fix(num200Over)
    if (newOrderType === "fullServiceAssembly") {
        return {
            fee: fullService_Fee,
            first: fullService_fir_add,
            second: fullService_sec_add,
            third: fullService_thi_add
        }
    }
    let assemblyFee = fix(assemblyCost) + 40
    let assembly_fir_add = fix(aIHEBBFU200lbs) * fix(num200Under) + fix(aIHEBBFO200lbs) * fix(num200Over)
    let assembly_sec_add = fix(aIHEB2FU200lbs) * fix(num200Under) + fix(aIHEB2FO200lbs) * fix(num200Over)
    let assembly_thi_add = fix(aIHEB3FU200lbs) * fix(num200Under) + fix(aIHEB3FO200lbs) * fix(num200Over)
    if (newOrderType === "assembly") {
        return {
            fee: assemblyFee,
            first: assembly_fir_add,
            second: assembly_sec_add,
            third: assembly_thi_add
        }
    }

    let deliveryFee = fix(fBD) + fix(eAB) * (fix(numberOfBoxes || 1) - 1) + fix(eABO200lbs) * fix(num200Over)
    let delivery_fir_add = fix(iHEBBFU200lbs) * fix(num200Under) + fix(iHEBBFO200lbs) * fix(num200Over)
    let delivery_sec_add = fix(iHEB2FU200lbs) * fix(num200Under) + fix(iHEB2FO200lbs) * fix(num200Over)
    let delivery_thi_add = fix(iHEB3FU200lbs) * fix(num200Under) + fix(iHEB3FO200lbs) * fix(num200Over)
    return {
        fee: deliveryFee,
        first: delivery_fir_add,
        second: delivery_sec_add,
        third: delivery_thi_add
    }
}

export const fix = (price) => {
    if (!price) return 0
    let te = price.toString();
    let temp = te.replace("$", "")
    return parseFloat(temp)
}
